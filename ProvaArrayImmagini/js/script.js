/**
 * @author Simone Tugnetti
 */


var c=0;
var display=document.getElementById("contatore");
var cornice=document.getElementById("img_slide");
var didascalia=document.getElementById("titolo_foto");
var miniature=document.getElementById("miniature");

var img=[
    'https://www.fiat.it/content/dam/fiat/cross/models/500_family/family_page/gallery/500C/02_gallery_A.jpg.transform/width-960/img.jpg',
    'https://assets.bugatti.com/fileadmin/_processed_/sei/p54/se-image-4d7e0aefef47f54a3b7f7c4c8d619141.jpg',
    'https://hips.hearstapps.com/amv-prod-cad-assets.s3.amazonaws.com/vdat/submodels/bmw_m8-gran-coupe_bmw-concept-m8-gran-coupe_2018-1532968589970.jpg',
    'http://cdn.auto.it/images/2018/06/05/113425951-41d6b5c1-53c6-4ce5-8f9b-e1b87a8c2f03.jpg'
];

var calciatore1={
    nome: "Ronaldo",
    foto: "img/ronaldo.jpg",
    gol: 400
};

var calciatore2={
    nome: "Marchisio",
    foto: "img/marchisio.jpg",
    gol: 250
};

var calciatore3={
    nome: "Cutrone",
    foto: "img/cutrone.jpg",
    gol: 300
};

var calciatore4={
    nome: "Modric",
    foto: "img/modric.jpg",
    gol: 100
};

var calciatore5={
    nome: "Treseguet",
    foto: "img/treseguet.jpg",
    gol: 250
};

var calciatori=[
    calciatore1,calciatore2,calciatore3,calciatore4,calciatore5
];

function next(){
    if(c<calciatori.length){
        c++;
        console.log("next ",c);
        mostraCalciatori();
    }
}

function prev(){
    if(c>0){
        c--;
        console.log("prev ",c);
        mostraCalciatori();
    }
}

function miniaturizza(){
    var calciatore;
    for(let i=0;i<calciatori.length;i++){
        calciatore=calciatori[i];
        let miniCalciatore=document.createElement("img");
        miniCalciatore.setAttribute("src",calciatore.foto);
        miniature.appendChild(miniCalciatore);
        console.log("calciatore --> ",calciatore);
    }
}

miniaturizza();

function mostra(){
    cornice.src=img[c];
}

function mostraCalciatori(){
    cornice.src=calciatori[c].foto;
    didascalia.innerHTML="<h4>"+calciatori[c].nome+"</h4>";
}
