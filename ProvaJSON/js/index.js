/**
 * @author Simone Tugnetti
*/

const aula = [
    new Studente("Giuseppe","Verdi"),
    new Studente("Carlo","Grimaldi"),
];
/*
$(document).ready(
    function(){
        aula.forEach(stud => {
            //console.log(stud.toString());
            //console.log($("h1").text());
            $("h1").after("<h2>"+stud.cognome+"</h2>");
        });
    }
);
*/
const elencoStudenti=[
    {"nome":"Giuseppe","cognome":"Verdi","_matricola":1},
    {"nome":"Carlo","cognome":"Grimaldi","_matricola":2}
];

console.log(JSON.stringify(aula));
$(document).ready(
    function(){
        elencoStudenti.forEach((st,i) => {
            let box=$("<div>");
            $(box).text(i+" "+st.nome+" "+st.cognome);
            $(box).width(100).height(100);
            $(box).css("border","solid black 1px");
            $(box).css("float","left");
            if(i%5==0){
                $(box).css("clear","left");
            }
            $("body").append(box);
            /*
            $("h1").after("<h2>"+st.cognome+"</h2>");
            console.log(st.cognome);
            */
        });
    }
);
