/**
 * @author Simone Tugnetti
*/

class Studente{
    constructor(nome,cognome){
        this.nome=nome;
        this.cognome=cognome;
        this.immatricola();
        this._matricola=Studente.matricola;

        console.log("Studente costruito!");
    }
    immatricola(){
        Studente.matricola++;
        console.log("Studente immatricolato!");
    }
}

Studente.matricola=0;

Studente.prototype.toString = function(){
    return `Nome: ${this.nome}, Cognome: ${this.cognome}, Matricola: ${this._matricola}`;
};