/**
 * @author Simone Tugnetti
*/


//JQuery = $

$("document").ready(
    function(){
        console.log("Funziona");
        const p = $("p:first").css({backgroundColor:"yellow",width:"90%",height:"150px",color:"red"});
        console.log(p);
        $("p:first").html("<h3>ciao</h3>").addClass("red");
        $("p:last").html("<h4>Buona giornata</h4>").addClass("blue").css("color","blue");
        const li = "<li>:last</li><li>:first</li><li>even</li>";
        $("#jp_filters").html(li);
        $("#jp_filters li:not(li:gt(1))").css("color","green");
        $("#jp_filters li:contains('s')").css("color","purple");
        $("li").click(function(){
            console.log(this);
            $(this).parent().next().next().css("font-size","+=1em");
        });

        $("li:eq(1)").attr("id","primo");
        $("ul").find("#primo").css("background-color","yellow");
        $("li").appendTo("li:eq(1)");

        let img = $("<img>");
        $(img).attr("src","");
        $("p:last").append(img);

        $("button:first").click(function(){
            $(img).animate({left:"1000"})
            $("p:last").hide(1500).show(2000);
            let nuovoLi=$("<li>");
            $(nuovoLi).text($("#fldScelta").val().appendTo("#jq_filters"));
        });

        $("p").text("lorem ipsum").after("ul");
        $("p").wrap("<div call='red'></div>");
        console.log($("p:last").width());
        console.log($("p:last").height());
        console.log($("p:last").offset().top);

        let collezione = $("li");
        $(collezione).each(function(){
            let check = $("input");
            $(check).attr("type","checkbox");
            this.prepend(check);
        });

        $("#fldScelta").on("keypress",function(){
            console.log($(this).val());
        });

        $(window).resize(function(){
            $("body").prepend("<div>"+$(window)+"</div>");
        });

        //$("#jp_filters li:eq(1)").css("font-size","+=1em");
    }
);