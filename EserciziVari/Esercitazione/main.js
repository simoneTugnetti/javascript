/**
 * @author Simone Tugnetti
*/


var nome = prompt("Qual'è il tuo nome?");

if(nome == "" || nome == null) {
    nome = "Utente anonimo";
}

document.getElementById("titolo").innerHTML = "Benvenuto "+nome;

var date = new Date().getHours();

if(date >= 8 && date <= 12) {
    document.getElementById("date").innerHTML = "Buongiorno";
} else if(date >=13 && date <= 20) {
    document.getElementById("date").innerHTML = "Buon pomeriggio";
} else {
    document.getElementById("date").innerHTML = "Buona notte";
}



function formCheck() {
    var username = document.forms['form']['username'].value;
    var psw = document.forms['form']['password'].value;
    if(psw.length < 5 || username == ""){
        alert("Inserire i dati corretti prima di procedere");
        return false;
    } else {
        return true;
    }
}




function changeBodyColorBtn(btn) {
    var color = document.getElementById(btn).style.backgroundColor;
    document.body.style.background = color;
}

function changeBodyColorList() {
    var form = document.getElementById("form2");
    var e = document.forms['form2']['colors'];
    var strUser = e.options[e.selectedIndex].value;
    if(strUser == "altro" && document.getElementById('color-altro') == null) {
        var input = document.createElement("input");
        input.id = 'color-altro';
        input.type = 'text';
        input.placeholder = 'Inserisci il colore';
        form.appendChild(input);
    } else {
        document.body.style.background = strUser;
    }
}