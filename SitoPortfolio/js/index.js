// JavaScript Document

/**
 * @author Simone Tugnetti
*/

function navigaSmooth(id){
	$("html, body").animate({scrollTop: $('#'+id).offset().top}, 650);
}

$(document).ready(
	function(){
		createTitle();
		setParallax();
		createAboutMe();
		createSkill();
		createFooter();
		createPortfolio();
		createFancybox();
	}
);

$(window).scroll(function(){
    var scrollVericale = $(this).scrollTop();
    $(".muovi").css({
        "transform" : "translate(0px, " + scrollVericale / 10 + "%)"
    });
    $(".macL").css({
        "transform" : "translate(0px, " + ( scrollVericale / 6) + "%)"
    });
	$(".macR").css({
        "transform" : "translate(0px, " + ( scrollVericale / 6) + "%)"
    });
	scrollTornaSu(scrollVericale);
});

function setParallax(){
	$.get("js/Tugnetti.json", function(io){
		$(".box").css({"backgroundImage": "url(./img/"+io.elevator.Box+")"});
		$('.macL').css({"backgroundImage": "url(./img/"+io.elevator.MacL+")"});
		$('.macR').css({"backgroundImage": "url(./img/"+io.elevator.MacR+")"});
		$("#elevatorPitch #utente").text(io.nome+' '+io.cognome);
		$("#elevatorPitch #descrizione").text(io.elevator.Descrizione);
	});
}

function createTitle(){
	$.get("js/Tugnetti.json", function(io){
    	$('nav #titolo').text(io.nome+' '+io.cognome+' - '+io.jobTitle);
	});
}


function createSkill(){
	
	const COLORS = ["green", "blue", "red", "purple"];
	
	$.get("js/Tugnetti.json", function(io){
		for(const comp of io.skill){
			skiller(comp.competenza, comp.voto, COLORS[Math.floor(Math.random()*COLORS.length)]);
     	}
	});
}

function skiller(competenza, voto, background){

	const BARRA = $("<div>");
	
    $(BARRA)
        .css({
				width: "25%",
                height: "3em", 
                backgroundColor: background, 
                color: "white", 
                padding: "0.5em", 
                marginBottom: "10px",
                fontSize: "1.3rem",
				borderBottomRightRadius: "5px",
				borderTopRightRadius: "5px"
            })
            .text(competenza)
            .appendTo("#skill");
	
	$("#skill").on("click", function(){
		$(BARRA).animate({width: ((voto*100)/30)+"%"}, 1000);
	});
	
	$("#link-skill").on("click", function(){
		$(BARRA).animate({width: ((voto*100)/30)+"%"}, 1000);
	});

}

function createPortfolio(){
	$.get("js/Tugnetti.json", function(io){
		for(const gallery of io.portfolio){
			setPortfolio(gallery.titolo, gallery.immagine, gallery.descrizione);
		}
	});
}

function setPortfolio(titolo, immagine, descrizione){
	var html = "<div class='card'>";
	html += "<a data-fancybox='photo' href='img/"+immagine+"'>";
	html += "<img src='img/"+immagine+"' class='card-img-top img-fluid' alt='ImageProgetto'>";
	html += "</a>";
	html += "<div class='card-body'>";
	html += "<h5 class='card-title'>"+titolo+"</h5>";
	html += "<p class='card-text'>"+descrizione+"</p>";
	html += "</div>";
	html += "</div>";
	$("#galleria").append(html);
}


function createAboutMe(){
	$.get("js/Tugnetti.json", function(io){
		var i=1;
		for(const sogg of io.aboutMe){
			abouter(sogg.immagine, sogg.titolo, sogg.paragrafo, i);
			i=i+1;
     	}
	});
}
		
function abouter(immagine, titolo, paragrafo, num){
	$("#aboutMeI"+num).css({backgroundImage: "url(./img/"+immagine+")"});
	$("#aboutMeT"+num).text(titolo);
	var paragrafoMod = $("#aboutMeP"+num).text(paragrafo);
	paragrafoMod.html(paragrafoMod.html().replace(/\n/g,'<br/>'));
}

function createFooter(){
	$.get("js/Tugnetti.json", function(io){
		$("#privacy").text(io.footer);
	});
}

function verificaDati(){
	let testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
	if($("#mail").val()==='' || $("#oggetto").val()==='' || $("#descrizioneForm").val()===''){
		alert("Alcuni campi risultano non compilati!");
	}else if(!testEmail.test($("#mail").val())){
		alert("Il campo E-mail risulta incorretto!");
	}else{
		alert("I campi risultano correttamente compilati!");
	}
}

function scrollTornaSu(scrollVert) {
  	if (scrollVert > 20) {
		$("#tornaSu").fadeIn();
  	}else {
    	$("#tornaSu").fadeOut();
  	}
}

function tornaInizio() {
	$("html, body").stop().animate({scrollTop:0}, 500, 'swing');
}

function createFancybox(){
	$("[data-fancybox='photo']").fancybox({
		slideShow: {
			autoStart: true,
			speed: 3000,
		}, loop: true
	});
}

