/** IIFE --> Funzioni immediatamente eseguibili */

/**
 * @author Simone Tugnetti
 */

let contatore = (function(){
    let conteggio=0;
    return function(){
        return `Il valore del contatore è: ${conteggio++}`;
    }
})();

console.log(contatore());
console.log(contatore());
console.log(contatore());
console.log(contatore());
console.log(contatore());

(function(){
    console.log("IIFE funge");
})();

const numeri=[-1,1,2,5,-8];

const nomi=[{nome:"mauro"},{nome:"carlo"},{nome:"giovanni"}];

//Anonymous function: argomento del foreach
numeri.forEach(function(x){
    console.log(x);
});

//Arrow function //Java lambda Expression
nomi.forEach((x) => {console.log(x.nome)});
const nome=nomi.filter(x => x.nome.length===5).forEach(x => console.log(x.nome));
console.log(nome);