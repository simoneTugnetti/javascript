console.log("Hello World");

/**  Variable Scope */

/**
 *  @author Simone Tugnetti
*/

//Dichiaro una fun

function f(x){
    return x/2;
}

function f2(x){
    console.log("E' stato passato il valore "+x);
}

console.log(f(2));
f2(5);

/**  Global e block scope */
let nome="Carlo";
let matricola=4;

function saluta(){
    let nome="Giovanni";
    console.log(`${nome} saluta tutti!
    può estendersi tranquillamente su diverse righe`);
}
console.log(nome);
saluta();

console.log("sono prima del block");
{
    const PIPPO="paperino";
    console.log(`Sono dentro il blocco ${PIPPO}`);
}
console.log("sono dopo il block");
//console.log(PIPPO);