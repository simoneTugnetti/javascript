/**
 * @author Simone Tugnetti
*/

$(document).ready(
    function () {

        const COLORS = ["green", "blue", "red", "purple"];

        //Leggo il JSON
        $.get("js/Tugnetti.json", function(io){
            //console.log(io.skill[0].competenza);
            for(const comp of io.skill){
                console.log(comp);
                skiller(comp.competenza, comp.voto, COLORS[Math.floor(Math.random()*COLORS.length)]);
            }
        });

        $("#elevator img").animate({left: "150px"}, 1000).animate({top: "150px"}, 1000);
        //$("#elevator img").animate({left: "150px", top: "150px"}, 1000);
    }

);

function skiller(competenza, voto, background){

    const bar = $("<div>");

    $(bar)
        .css({
                height: "3em", 
                backgroundColor: background, 
                color: "white", 
                padding: "0.5em", 
                marginBottom: "10px",
                fontSize: "1.3rem" 
            })
            .text(competenza)
            .animate( {width: voto+"%"}, 1000)
            .appendTo("#skill");
}