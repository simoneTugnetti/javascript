/**
 * @author Simone Tugnetti
*/


var titolo;

$("#formTitolo").submit(function(e) {
    e.preventDefault();
    if($("#titoloFilm").val() == "") {
        alert("Inserire un titolo");
    } else {
        titolo = "http://api.tvmaze.com/search/shows?q="+$("#titoloFilm").val().replace(/ /g,"-");
    }
    if(titolo != null) {
        $("#tableFilms").empty();
        $("#tableFilms").append("<tr><td>Titolo</td><td>Generi</td><td>Rating</td></tr>");
        $.getJSON(titolo, function(films){
            console.log(films);
            var scoreArray = [];
            films.forEach(element => {
                scoreArray.push(element.score);
            });
            var scoreOrd = orderArray(scoreArray);
            for(var i=0;i<films.length; i++) {
                var name = films[i].show.name.replace(/ /g,"-");
                for(var j=0; j<scoreOrd.length; j++){
                    if(scoreOrd[j] == films[i].score){
                        var generi = films[i].show.genres.join(",");
                        $("#tableFilms").append("<tr><td id="+name+"></td><td>"+generi+"</td><td>"+scoreOrd[j]+"</td></tr>");
                        $("#"+name).append("<a href='#' id='alert"+name+"'>"+films[i].show.name+"</a>");
                    }
                }
                $("#alert"+name).click(function(){
                    alert("dati");
                })
            }
        });
    }
});



function orderArray(array){
    var arrayOrd = array.sort();
    return arrayOrd.reverse();
}