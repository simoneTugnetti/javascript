/**
 * @author Simone Tugnetti
*/


console.log("ToDo list");
class Task{
    constructor(titolo){
        this.titolo=titolo;
        this.data=new Date();
        console.log("Task costruito!");
    }
}

let task=new Task("Hello world");
let task2=new Task("Hello continent");
let task3=new Task("Hello state");

const INPUT=document.createElement("input");

INPUT.setAttribute("type","text");
INPUT.setAttribute("placeholder","To Do");
document.body.appendChild(INPUT);

const UL=document.createElement("ul");
document.body.appendChild(UL);

let elenco=[task,task2,task3];

function stampaElenco(){

    UL.innerHTML="";

    elenco.forEach(task => {
        console.log(task.titolo);
        const LI=document.createElement("li");
    
        let testo=document.createTextNode(task.titolo);

        LI.addEventListener("click",function(evento){
            console.log("Hai cliccato su "+evento);
            evento.target.style.color="red";
        });
    
        LI.addEventListener("dblclick",function(evento){
            console.log("Hai cliccato su "+evento);
            evento.target.style.color="black";
        });

        LI.appendChild(testo);
        UL.appendChild(LI);
    });
}

INPUT.onblur=function(evento){
    console.log(evento.target.value);
}

INPUT.onkeypress=function(evento){
    //console.log(evento.code);
    if(evento.code=="Enter"){
        let bersaglio=evento.target.value;
        console.log(bersaglio);
        let tmp=new Task(bersaglio);
        elenco.push(tmp);
        console.log(elenco);
        stampaElenco();
    }
}